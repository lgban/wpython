from helpers.helpers import OutputMixin
from sqlalchemy import create_engine, Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:////tmp/test.db', convert_unicode=True)
Session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         enable_baked_queries=True,
                                         bind=engine))

session = Session()

Base = declarative_base()
Base.query = Session.query_property()

class PlantInventoryEntry(Base, OutputMixin):
    __tablename__ = "plant_inventory_entry"
    id = Column(Integer, primary_key=True)
    name = Column(String(80))
    description = Column(String(256))
    price = Column(Integer)

    def __repr__(self):
        return '<PIEntry %r>' % self.name


class PlantInventoryItem(Base, OutputMixin):
    __tablename__ = "plant_inventory_item"
    id = Column(Integer, primary_key=True)
    serial_number = Column(String(10), unique=True, nullable=False)
    catalog_entry_id = Column(Integer, ForeignKey('plant_inventory_entry.id'))
    equipment_condition = Column(String(20), nullable=False)

    def __repr__(self):
        return '<PIItem %r>' % self.serial_number


class PurchaseOrder(Base, OutputMixin):
    __tablename__ = "purchase_order"
    id = Column(Integer, primary_key=True)
    start_date = Column(Date())
    end_date = Column(Date())
    catalog_entry_id = Column(Integer, ForeignKey('plant_inventory_entry.id'))
    plant_id = Column(Integer, ForeignKey('plant_inventory_item.id'))

    def __repr__(self):
        return '<PO %d>' % self.id

def query_catalog(name):
    plants = PlantInventoryEntry.query.filter(
        PlantInventoryEntry.name.ilike("%" + name + "%"),
        PlantInventoryEntry.price < 300
    ).all()

    # plants = session.query(PlantInventoryEntry).join(PlantInventoryItem).all()
    return list(map(lambda e: e.to_dict(), plants))

def query_purchase_orders(start_date, end_date):
    # orders = PurchaseOrder.query.filter(
    #     start_date < PurchaseOrder.end_date,
    #     end_date > PurchaseOrder.start_date
    # ).all()

    subquery1 = session.query(PurchaseOrder.plant_id).filter(
        start_date < PurchaseOrder.end_date,
        end_date > PurchaseOrder.start_date
    )

    subquery2 = session.query(PlantInventoryItem.catalog_entry_id).filter(
        ~PlantInventoryItem.id.in_(subquery1)
    )

    plants = session.query(PlantInventoryEntry).filter(
        PlantInventoryEntry.description == "1.5 Tonne Mini excavator",
        PlantInventoryEntry.id.in_(subquery2)
    )

    return list(map(lambda e: e.to_dict(), plants))