from flask import Flask
from flask import jsonify
from flask_cors import CORS
from flask import request
import datetime
from models.sales import query_catalog, query_purchase_orders

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
    return jsonify({"msg": "Hello world"})

@app.route('/catalog')
def query1():
    name = request.args.get('name')
    return jsonify(query_catalog(name))

@app.route('/orders')
def query2():
    return jsonify(query_purchase_orders(datetime.date(2019, 5, 5), datetime.date(2019, 5, 11)))