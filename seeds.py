from models.sales import PurchaseOrder
from models.sales import PlantInventoryEntry, PlantInventoryItem
from models.sales import Base, session, engine
import datetime

entries = [
    PlantInventoryEntry(id=1, name='Mini excavator', description='1.5 Tonne Mini excavator', price=100),
    PlantInventoryEntry(id=2, name='Mini excavator', description='3 Tonne Mini excavator', price=200),
    PlantInventoryEntry(id=3, name='Midi excavator', description='5 Tonne Midi excavator', price=250),
    PlantInventoryEntry(id=4, name='Midi excavator', description='8 Tonne Midi excavator', price=300),
    PlantInventoryEntry(id=5, name='Maxi excavator', description='15 Tonne Large excavator', price=400),
    PlantInventoryEntry(id=6, name='Maxi excavator', description='20 Tonne Large excavator', price=450),
    PlantInventoryEntry(id=7, name='HS dumper', description='1.5 Tonne Hi-Swivel Dumper', price=150),
    PlantInventoryEntry(id=8, name='FT dumper', description='2 Tonne Front Tip Dumper', price=180),
    PlantInventoryEntry(id=9, name='FT dumper', description='2 Tonne Front Tip Dumper', price=200),
    PlantInventoryEntry(id=10, name='FT dumper', description='2 Tonne Front Tip Dumper', price=300),
    PlantInventoryEntry(id=11, name='FT dumper', description='3 Tonne Front Tip Dumper', price=400),
    PlantInventoryEntry(id=12, name='Loader', description='Hewden Backhoe Loader', price=200),
    PlantInventoryEntry(id=13, name='D-Truck', description='15 Tonne Articulating Dump Truck', price=250),
    PlantInventoryEntry(id=14, name='D-Truck', description='30 Tonne Articulating Dump Truck', price=300),
]

plants = [
    PlantInventoryItem(id=1, catalog_entry_id=1, serial_number='A01', equipment_condition='SERVICEABLE'),
    PlantInventoryItem(id=2, catalog_entry_id=1, serial_number='A02', equipment_condition='SERVICEABLE'),
    PlantInventoryItem(id=3, catalog_entry_id=2, serial_number='A03', equipment_condition='SERVICEABLE')
]

pos = [
    PurchaseOrder(id=1, catalog_entry_id=1, plant_id=1, start_date=datetime.date(2019, 5, 6), end_date=datetime.date(2019, 5, 10))
]


def seed_db():
    session.bulk_save_objects(entries)
    session.bulk_save_objects(plants)
    session.bulk_save_objects(pos)
    session.commit()

def init_db():
    Base.metadata.create_all(bind=engine)